function getTarefas(){
	this.itens = [];

	var lista = localStorage.getItem('listaTarefas');

	if(lista !== null){
		this.itens = angular.fromJson(lista);
	}

	this.salvar = function(){

		var lista = angular.toJson(this.itens);

		localStorage.setItem('listaTarefas', lista);


	}

	this.add = function(item){
		this.itens.push(item);
	};

	this.remover = function(item){
		var posicao = this.itens.indexOf(item);
		this.itens.splice(posicao, 1);
	};
}