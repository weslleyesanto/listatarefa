// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var app = angular.module('starter', ['ionic']); // criando aplicação angular (pagina, required)

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

app.controller('mainController', function($scope, $ionicPopup, $ionicListDelegate){
  var tarefas = new getTarefas();

  $scope.lista = tarefas.itens;
  $scope.exibirMarcador  = false;
  $scope.removeStatus = false;

  function popupAddEditar(item, novo){
    $scope.data = {};
    $scope.data.novaTarefa = item.nome;

    $ionicPopup.show({
      title: 'Nova tarefa',
      scope: $scope,
      template: '<input type="text" placeholde="Tarefa" autofocus="true" ng-model="data.novaTarefa"/>',
      buttons: [
            {text: 'Ok', 
              onTap: function(e){
                item.nome = $scope.data.novaTarefa;

                if(novo){
                  tarefas.add(item);
                }
                
                tarefas.salvar();

              }},
            {text: 'Cancelar'}
            ],
    });

    $ionicListDelegate.closeOptionButtons();
  }

  $scope.marcarTarefa = function(item){
    item.finalizada = !item.finalizada;
    tarefas.salvar();

  };

  $scope.onHideItem = function(item){
    return item.finalizada && !$scope.exibirMarcador;
  }

  $scope.onItemAdd = function(){

    var item = {nome: "", finalizada: false};
    popupAddEditar(item, true);

  };

  $scope.onItemRemove = function(item){
    tarefas.remover(item, false);
    tarefas.salvar();
  }

  $scope.onClickRemove = function(){
    $scope.removeStatus = !$scope.removeStatus;
  }

  $scope.onItemEditar = function(item){
    popupAddEditar(item);
    tarefas.salvar();
  }

})

